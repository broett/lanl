# Kaggle Competition [LANL-Earthquake-Prediction](https://www.kaggle.com/c/LANL-Earthquake-Prediction)

Basic tools, information and models for the [LANL-Earthquake-Prediction](https://www.kaggle.com/c/LANL-Earthquake-Prediction)
challenge on [Kaggle](https://www.kaggle.com).

## 1. Usage

To use this package within a Kaggle kernel run this code from within the kernel:
```python
!pip install --upgrade pip
!pip install git+http://bitbucket.org/broett/lanl@master#egg=lanl
```
The `@master`, specifying the branch, as well as the `#egg=lanl`, specifying the module's name, are optional.
After that, you can just simply import the module as usual:
```python
import lanl
```

## 2. Data

The training data consist of 629,145,480 lines / data points with a column for `acoustic data` (in the range -5,515 to 5,444)
and one column of the `time to failure`, the target variable.
The test data is 2,624 individual chunks of 150,000 acoustic data points.

Although there is actually one big chunk of training data, dividing it into consequitive chunks of length 150,000 yields 4,194.3 training samples.
Therefore, there is just a moderate amount of training data.
Splitting the data into overlapping sequences, can alleviate the problem a little.

However, there are also only 16 failures (i.e. quakes) in the training data, which gives only 17 groups.
It seems *(does it really? or is it just an assumption)*, that each of these groups is structually a little different from the others,
and hence, the test data.
Moreover, due to the different lengths of the 17 groups, there is different amount of training data for different `time_to_failure` values.
Except for the first (1.47 s) and last (1.86 s), the groups have lengths between 7 s and a bit more than 16 s.

This makes - besides physical reasoning - higher `time_to_failure` values harder to predict.
Also, there are some 'mini quakes' between two failures which further makes high values hard to predict,
as they seem to be short before a quake (actually they are short for the mini quake), but the actual
quake is still far away,
Small values (roughly less then one) seem quite hard to predict, too.

## 3. More Data

- there is another data set on Kaggle now for training: [p4581](https://www.kaggle.com/leighplt/laboratory-acoustic-data-exp4581) it is said to have the sane set-up
- see [this discussion](https://www.kaggle.com/c/LANL-Earthquake-Prediction/discussion/77526) on time resolution, though
- cannot use the data as it is / only use a part of it (see [here](https://www.kaggle.com/leighplt/laboratory-ac-data-exp-p4581))

## 4. Plan

Time is short now. I need to focus.
Hence, do not try totally different approaches (like NN on raw data, auto-encoders for feature engeneering, RNN, ...).
This despite the fact that CNN's [seem to work](https://www.kaggle.com/pedrormarques/signal-convolution-v4).
Do not invest much time to lean new things, though!

1. **Rething** this plan before you start. *Some* well-chosen parts should be put in functions, but *not everything*!
2. Think again about CV, then **commit to** *one* reproducible **CV**. It should:
	- not leak (between groups, between overlapping segments, ...)
	- have similar TTF distributions per split
	- be reproducable (same or at least very stable results, if done twice with different seed)
	- probably take the GOOD_SPLITS of the groups...?
3. Start with the current feature table, i.e. **create the feautures and save** them. Do not use them directly.
4. **Start with at most 3 good looking features** (by eye!)...
5. ... and **one model** (e.g. XGBoost) with fixed(!) hyper-parameters (tuning only at the end with stable features).
6. **Select features one by one** (and risk overseeing pairs that might be good - maybe do some loop at the end).
	- write a loop (and maybe try a few features chosen by hand in more depth)
	- if it correlates (transformed) with one already selected features highly, test which one is better by CV
	- test if adding feature to model improves CV score
	- also have a look at this [researchgate publication](https://www.researchgate.net/publication/221996079_An_Introduction_of_Variable_and_Feature_Selection)
	- also check, that features have a similar distribution in train and test set
7. Create **more features**
	- for ideas look at [this kernel](https://www.kaggle.com/pestipeti/signal-and-frequency-data-animation)
	- plot segments and FFT's (or Welch transform or periodogram) for different parts of the data
8. **Tune hyper-parameters**, models and things to test:
	- substracting mean or not
	- target transformation
	- other objectives than MAE
	- `XGBRegressor`, LightGBM, `RandomForestRegressor`, [`gplearn`](https://gplearn.readthedocs.io/en/stable/)`.SymbolicRegressor`, ...
	- BaggingRegressor
9. **Ensemble** and final submit

## 5. New Tricks

- same submission files with CV score (and maybe also training score because of overfitting)