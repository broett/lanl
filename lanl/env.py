__all__ = [
    'init_kaggle_api', 'get_done_submissions', 'get_leaderboard', 'submit',
    'type_of_script', 'tqdm',
]

import kaggle, io, subprocess, os, tempfile
import pandas as pd

KAGGLE_API = None
COMPETITION = 'lanl-earthquake-prediction'

def type_of_script(): 
    from IPython import get_ipython 
    try: 
        ipy_str = str(type(get_ipython())) 
        if 'zmqshell' in ipy_str: 
            return 'jupyter' 
        if 'terminal' in ipy_str: 
            return 'ipython' 
        return 'unknown' 
    except: 
        return 'terminal'

if type_of_script() == 'jupyter':
    from tqdm import tqdm_notebook as tqdm
else:
    from tqdm import tqdm

def init_kaggle_api(reinit=False):
    global KAGGLE_API
    if KAGGLE_API is None or reinit:
        KAGGLE_API = kaggle.KaggleApi()
        KAGGLE_API.authenticate()

def get_done_submissions(competition=COMPETITION):
    init_kaggle_api()
    submissions = KAGGLE_API.competition_submissions(competition)
    subs_dict = [
        { attr:getattr(sub,attr) for attr in dir(sub) if not attr.startswith('__') }
        for sub in submissions
    ]
    return pd.DataFrame(subs_dict)

def get_leaderboard(competition=COMPETITION):
    init_kaggle_api()
    with tempfile.TemporaryDirectory() as temp:
        KAGGLE_API.competition_leaderboard_download(competition, temp)
        path = os.path.join(temp, competition+'.zip')
        csv = subprocess.check_output(['tar', '-x', '-f', path, '-O'])
        csv_io = io.StringIO(csv.decode('utf-8'))
        LB = pd.read_csv(csv_io)
    return LB.sort_values('Score')

def submit(fname, msg, competition=COMPETITION, quite=False):
    init_kaggle_api()
    KAGGLE_API.competition_submit(fname, msg, competition, quiet=quite)

