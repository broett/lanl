__all__ = [
        'TRAIN_CSV', 'SAMPLE_SUBMISSION_CSV',
        'TRAIN_ROWS', 'SEG_LEN', 'N_GRP', 'GRP_INFO',
        'get_meta_of_train_idx', 'print_info',
]

import numpy as np
import pandas as pd

TRAIN_CSV = '../input/LANL-Earthquake-Prediction/train.csv'
SAMPLE_SUBMISSION_CSV = '../input/LANL-Earthquake-Prediction/sample_submission.csv'
TEST_DIR = '../input/LANL-Earthquake-Prediction/test/'

# training segments
N_TEST_SEG = 2_624
# number of data rows in `train.csv` (that's excluding the header line)
TRAIN_ROWS = 629_145_480
SEG_LEN = 150_000
# indices / rows of failures / quakes
QUAKE_IDX = np.array([  5_656_574,  50_085_878, 104_677_356, 138_772_453, 187_641_820, 218_652_630,
                      245_829_585, 307_838_917, 338_276_287, 375_377_848, 419_368_880, 461_811_623,
                      495_800_225, 528_777_115, 585_568_144, 621_985_673])
GRP_EDGES = np.concatenate(([0],QUAKE_IDX,[TRAIN_ROWS]))
GRP_LENS = np.diff(GRP_EDGES)
N_GRP = len(GRP_LENS)
FIRST_TTF = np.array([ 1.4690999984741211, 11.5408000946044922, 14.1806001663208008,
                       8.8566999435424805, 12.6940002441406250,  8.0555000305175781,
                       7.0590000152587891, 16.1074008941650391,  7.9056000709533691,
                       9.6371002197265625, 11.4264001846313477, 11.0242004394531250,
                       8.8281002044677734,  8.5659999847412109, 14.7517995834350586,
                       9.4595003128051758, 11.6185998916625977])
LAST_TTF = np.array([  0.0007954798056744,  0.0006954821874388,  0.0007954850443639,
                       0.0010954869212583,  0.0005954894586466,  0.0010954911122099,
                       0.0004954925389029,  0.0006954958080314,  0.0001954974286491,
                       0.0004954994074069,  0.0001955017214641,  0.0000955039649853,
                       0.0000955057621468,  0.0008955075172707,  0.0005955105298199,
                       0.0005955124506727,  9.7597951889038086])

GRP_INFO = pd.DataFrame({
    'first_idx': GRP_EDGES[:-1],
    'last_idx': GRP_EDGES[1:]-1,
    'end_idx': GRP_EDGES[1:],
    'len': GRP_LENS,
    'first_TTF': FIRST_TTF,
    'last_TTF': LAST_TTF,
})

GRP_DUR = FIRST_TTF-LAST_TTF
dt = GRP_DUR / (GRP_LENS-1)
dt = np.mean(dt[1:-1]) # first segment (and also a bit the last one) is a short
GRP_DUR += dt
GRP_INFO['duration'] = GRP_DUR

GRP_INFO['mean_TTF'] = (FIRST_TTF + LAST_TTF) / 2
GRP_INFO['frac_duration'] = GRP_DUR / GRP_DUR.sum()

def get_meta_of_train_idx(i):
    g = np.where( (GRP_INFO['first_idx'] <= i) & (i < GRP_INFO['end_idx']) )[0][0]
    info = GRP_INFO.iloc[g]
    alpha = (i-info['first_idx']) / (info['last_idx']-info['first_idx'])
    ttf = alpha*info['last_TTF'] + (1-alpha)*info['first_TTF']
    return g, ttf

def print_info():
    print('The time resolution of the data is about {:.3f} micro-sec ({:.2f} MHz).'.format(dt*1e6, 1e-6/dt))
    print('The total length of the training sequence ({:,} data points) is about {:.0f}:{:02.0f} min.'.format(
        TRAIN_ROWS, np.floor(dt*TRAIN_ROWS/60), (dt*TRAIN_ROWS)%60))
    print('It contains {} quakes/failures (none at the beginning or end).'.format(N_GRP-1))
    print('The test set is {:,} individual segments of length {:,}.'.format(N_TEST_SEG, SEG_LEN))
    print('When splitting the training data into non-overlapping consequtive segments')
    print('of {:,} there are {:,} of them.'.format(SEG_LEN, TRAIN_ROWS//SEG_LEN))

