__all__ = ['load_random_seg', 'FeatureGenerator', 'DATA_FOLDER']

from .info import SEG_LEN, TRAIN_ROWS, GRP_INFO, GRP_LENS, GRP_EDGES
from .info import TRAIN_CSV, SAMPLE_SUBMISSION_CSV, TEST_DIR, N_GRP
from .info import get_meta_of_train_idx
from .features import make_feature_dict
from .preprocess import pre_process_seg
from .env import tqdm

import numpy as np
import pandas as pd
from scipy import signal
import gc, os, time
from joblib import Parallel, delayed, effective_n_jobs

DATA_FOLDER = '../input/bernis-lanl-features'

def load_random_seg(acoustic, length=SEG_LEN, excl_groups=None, incl_groups=None):
    '''
    Load a random segment of the given entire data set that does not contain a quake.

    Args:
        acoustic (np.ndarray):  This must be the entire(!) acoustic training data.
        length (int):           The length of the segment.
        excl_groups (iterable): Groups (i.e. some oth the 17 parts between quakes) to exclude.
        incl_groups (iterable): Only include those groups (although those from `excl_groups`
                                are still excluded).
                                If this is None, it defaults to all 17 groups,
                                i.e. `range(N_GRP)`.

    Returns:
        seg (np.ndarray[float64]):  The random acoustic segment.
        ttf (float):                The time to failure at the end of the segment.
        idx (int):                  The index in `acoustic` of the very last element in `seg`.
        group (int):                The group the segment was taken from.
    '''
    if not isinstance(acoustic, np.ndarray) or acoustic.shape != (TRAIN_ROWS,):
        raise ValueError("'acoustic' must be the entire acoustic training data as np.ndarray!")

    if excl_groups is None: excl_groups = []
    if incl_groups is None: incl_groups = range(N_GRP)
    excl_groups = set(excl_groups) | (set(range(N_GRP))-set(incl_groups))
    
    p = ((GRP_LENS-length)/TRAIN_ROWS).copy()
    if excl_groups:
        p[tuple(excl_groups),] = 0.0
    p /= np.sum(p)
    
    g = np.random.choice(range(N_GRP), p=p)
    end_point = np.random.randint(GRP_EDGES[g]+length, GRP_EDGES[g+1])
    
    seg = acoustic[end_point-length:end_point].astype(np.float64)
    group, ttf = get_meta_of_train_idx(end_point-1)
    assert group == g
    assert group in incl_groups
    assert group not in excl_groups
    
    return seg, ttf, end_point-1, group

class FeatureGenerator(object):
    def __init__(self, source, seg_len=SEG_LEN, stride=None, chunk_size=None):
        '''
        Feature generator.

        Args:
            source ('train', 'test'):   Whether to create the features for the training
                                        or testing data.
            seg_len (int):              The length of the segments to calculate the features on.
            stride (int):               The stride of the segments.
                                        If None, it defaults to `seg_len`.
            chunk_size (int):           The chunk size to read. Ignored for `source='test'`.
                                        If None, it defaults to `seg_len+stride`.
        '''
        if stride is None:
            stride = seg_len
        if chunk_size is None:
            chunk_size = seg_len + stride

        self._chunk_size = int(chunk_size)
        self._N_chunks = None
        self._seg_len = int(seg_len)
        self._stride = int(stride)
        self._source = source
        self._test_files = {}
        self._N_seg = None
        
        if self._source == 'train':
            if self._chunk_size < self._seg_len:
                raise ValueError('`chunk_size` needs to be at least twice the `seg_len`.')
            rem = TRAIN_ROWS % self._chunk_size
            self._N_chunks = int( np.ceil(TRAIN_ROWS // self._chunk_size) )
            self._N_seg = 0
            for group, g_data in GRP_INFO.iterrows():
                self._N_seg += (int(g_data['len']) - self._seg_len) // self._stride + 1
                if int(g_data['len']) < self._seg_len:
                    self._N_seg -= 1
        elif self._source == 'test':
            if self._seg_len > 150_000:
                print('WARNING: seg_len was larger than 150,000 - it is now set to 150,000.')
                self._seg_len = 150_000
            submission = pd.read_csv(SAMPLE_SUBMISSION_CSV)
            self._N_chunks = len(submission)
            self._N_seg = len(submission) * ( (SEG_LEN - self._seg_len) // self._stride + 1 )
            sed_ids = submission['seg_id'].values
            assert set([seg_id+'.csv' for seg_id in sed_ids]) == \
                    set([test for test in os.listdir(TEST_DIR)])
            self._test_files = {seg_id:os.path.join(TEST_DIR,seg_id+'.csv')
                               for seg_id in sed_ids}
        else:
            raise ValueError('`source` needs to be either "train" of "test"!')

    @property
    def source(self):
        '''Name of te source.'''
        return self._source
    
    def _seg_reader(self):
        '''Create a generator object that iterates over the source.'''
        if self._source == 'train':
            train_reader = pd.read_csv(
                TRAIN_CSV,
                dtype={'acoustic_data':np.float64, 'time_to_failure':np.float64},
                chunksize=self._chunk_size,
            )

            pre_chunk = None
            n = 0
            k0 = 0
            for chunk in train_reader:
                if pre_chunk is not None:
                    chunk = pd.concat([pre_chunk, chunk])
                    del pre_chunk

                was_split_by_group = False
                for g_idx in GRP_INFO['first_idx']:
                    if chunk.index[0] < g_idx and g_idx <= chunk.index[-1]:
                        pre_chunk = chunk.loc[g_idx:]
                        chunk = chunk.loc[:g_idx-1]     # weirdly enough, pandas includes the last index...
                        was_split_by_group = True
                        break

                dfs = []
                for k in range(k0, len(chunk)-self._seg_len+1, self._stride):
                    df = chunk.iloc[ k : k + self._seg_len ]
                    dfs.append( df )

                for df in dfs:
                    group, ttf = get_meta_of_train_idx(df.index[-1])
                    group_0, ttf_0 = get_meta_of_train_idx(df.index[0])
                    if group_0 != group: # would have a failure inside
                        assert False # should not happen, though, because of above test of indices

                    assert abs( df['time_to_failure'].values[-1] - ttf ) < 1e-2
                    ttf = df['time_to_failure'].values[-1] # more accurate than from `get_meta_of_train_idx`
                    seg = df['acoustic_data'].values
                    assert len(seg) == self._seg_len
                    
                    seg_id = 'train_{:06x}'.format(n)
                    n += 1
                    yield seg_id, seg, ttf, df.index[-1], group

                if was_split_by_group:
                    # `pre_chunk` already set at group split
                    k0 = 0
                else:
                    pre_chunk = chunk.iloc[ k : ]
                    k0 = self._stride
                    if len(pre_chunk) == 0:
                        pre_chunk = None
        else:
            for seg_id, fname in self._test_files.items():
                df = pd.read_csv(
                        fname,
                        dtype={'acoustic_data':np.float64, 'time_to_failure':np.float64},
                )

                s = (len(df) - self._seg_len) % self._stride
                for k in range(0,len(df)-self._seg_len+1,self._stride):
                    seg = df['acoustic_data'].iloc[ s + k : s + k + self._seg_len ]
                    yield seg_id, seg.values, -1, seg.index[-1], -1
    
    def _make_data_dict(self, make_feature_dict, x, ttf, seg_id, end_idx, group):
        data_dict = {
                'time_to_failure': ttf,
                'seg_id': seg_id,
                'end_idx': end_idx,
                'group': group,
        }

        data_dict.update( make_feature_dict(x) )
        
        return data_dict
    
    def generate(self, make_feature_dict=None, n_jobs='auto', backend='threading', verbose=True):
        '''
        Generate the features.

        Args:
            make_feature_dict (func):   Function that takes one sequence and returns a
                                        dictionary of scalar features.
            n_jobs (int, 'auto'):       The number of jobs to use. (If it equals one,
                                        do not invoke a parallel environment.)
                                        If it is 'auto', `n_jobs` is set to the most effective
                                        number of jobs.
            backend (str):              Argument for `joblib.Parallel`. If `n_jobs==1`
                                        (explicit or implicit by 'auto'), this argument is
                                        ignored.
            verbose (bool):             Controls verbosity.

        Returns:
            data (pd.DataFrame):    A pandas DataFrame with all the features as well as
                                    'time_to_failure', 'seg_id', 'group' for each segment.
        '''
        if n_jobs is 'auto':
            n_jobs = effective_n_jobs()

        if verbose:
            N = len( make_feature_dict(np.random.rand(self._seg_len)) )
            print(f'generate {self._N_seg:,} {self._source} segments of length {self._seg_len:,}')
            print(f'and generate {N} features for each segment')
            if n_jobs == 1:
                print(f'running single threaded')
            else:
                print(f'running with {n_jobs} threads')

        data = []
        pbar = tqdm(self._seg_reader(), total=self._N_seg)
        if n_jobs == 1:
            for s,x,y,i,g in pbar:
                data.append(self._make_data_dict(make_feature_dict,x,y,s,i,g))
        else:
            p = Parallel(n_jobs=n_jobs, backend=backend)
            res = p(delayed(self._make_data_dict)(make_feature_dict,x,y,s,i,g) for s,x,y,i,g in pbar)
            for r in res:
                data.append(r)
            del res

        if verbose: print('building pandas DataFrame')
        data = pd.DataFrame(data)

        gc.collect()
        if verbose: print('feature generation done.')
        return data

