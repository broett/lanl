import numpy as np

config = {
        'sub_mean': True,
        'wiener': True,
        'wiener_size': 51,
        'wiener_noise': 32,
        'wiener_return_noise': False,
}

def pre_process_seg(x, **args):
    '''
    Preprocess an acoustic segment.

    Args:
        x (array-like):             The segment to preprocess.
        sub_mean (bool):            Whether to substracact the mean from the
                                    acoustic data.
        wiener (bool):              Whether to apply a Wiener filter.
        wiener_size (int):          The size of the Wiener filter (`mysize` of
                                    `scipy.signal.wiener`).
        wiener_noise (int):         The noise-power of the Wiener filter (`noise`
                                    of `scipy.signal.wiener`).
        wiener_return_noise (bool): If True, also return the substracted noise.

    Returns:
        x (np.ndarray):     The pre-processed segment.
      [ noise (np.ndarray): If `wiener_return_noise` was True, the noise is
                            returned, two. ]
    '''
    p = config.copy()
    p.update(args)

    if p['sub_mean']:
        x = x - np.mean(x)

    if p['wiener']:
        from scipy.signal import wiener
        w = wiener(x, mysize=p['wiener_size'], noise=p['wiener_noise'])
        noise = x - w
        x = w
    else:
        noise = None
    
    if p['wiener_return_noise']:
        return x, noise
    else:
        return x

