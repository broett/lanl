__all__ = ['BEST_PARAMS', 'build_feature_NN']

from .features import N_FEATURES

# determined on `PowerTransform`ed 243 features:
BEST_PARAMS = {
        # CV = 1.955 +- 0.071
        # LB = 1.545
        'XGBRegressor': {
                'base_score':       5.66142,
                'colsample_bytree': 0.80606,
                'learning_rate':    0.05685,
                'max_depth':        4,
                'n_estimators':     90,
                'reg_lambda':       0.09814,
                'objective':        'reg:squarederror',
            },

        # CV = 1.??? +- 0.???
        # LB = 1.???
        'XGBRegressor - GPU': {
                'colsample_bytree': 0.84594,
                'learning_rate':    0.04160,
                'max_depth':        4,
                'n_estimators':     186,
                'reg_lambda':       0.11671,
                'objective':        'reg:squarederror',
            },

        # CV = 1.??? +- 0.???
        # LB = 1.???
        'LGBMRegressor': {
                'colsample_bytree': 0.83846,
                'learning_rate':    0.01618,
                'max_depth':        -1,
                'n_estimators':     208,
                'num_leaves':       30,
                'reg_lambda':       0.45848,
            },

        # CV = 1.??? +- 0.???
        # LB = 1.???
        'RandomForestRegressor': {
            },
}

def build_feature_NN(optimizer='adam', loss='mae',
                     input_shape=(N_FEATURES,),
                     activ='relu',
                     dense_neurons=(64,64,64),
                     dropouts=(0.25,0.25,0.1),
                     L1=1e-10, L2=1e-12):
    from keras.layers import Input, Dense, Dropout
    from keras.models import Model
    from keras.regularizers import l1_l2

    if len(dense_neurons) != len(dropouts):
        raise ValueError('`dense_neurons` and `dropouts` must be of same length!')
    
    inp = Input(input_shape)
    x = inp
    for n,do_rate in zip(dense_neurons,dropouts):
        x = Dense(n, activation=activ if isinstance(activ,str) else None,
                  kernel_regularizer=l1_l2(L1,L2),
                  activity_regularizer=l1_l2(L1,L2))(x)
        if not isinstance(activ,str):
            x = activ(x)
        if do_rate != 0:
            x = Dropout(do_rate)(x)
    ttf = Dense(1)(x)
    
    model = Model(inputs=inp, outputs=ttf)
    model.compile(optimizer=optimizer, loss=loss, metrics=['mae'])
    
    return model

