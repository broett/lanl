__all__ = [
        'scalar_features',
        'make_feature_dict',
        'N_FEATURES', 'FEATURES',
]

from .info import *
from .preprocess import pre_process_seg

import numpy as np
import pandas as pd
from scipy import stats, signal
import librosa
from .funcs import perm_entropy, calc_change_rate
from obspy.signal.trigger import classic_sta_lta, recursive_sta_lta, z_detect, carl_sta_trig
from tsfresh.feature_extraction import feature_calculators as fc
import librosa.feature as lf

def scalar_features(x, pre=''):
    feature_dict = dict()

    feature_dict[f'{pre}std'] = np.std(x)
    feature_dict[f'{pre}mean'] = np.mean(x)
    feature_dict[f'{pre}median'] = np.median(x)

    feature_dict[f'{pre}min'] = np.min(x)
    feature_dict[f'{pre}max'] = np.max(x)
    feature_dict[f'{pre}ptp'] = np.ptp(x)
    
    for p in [1,2,5,10,25]:
        feature_dict[f'{pre}perc{p:02}']     = low  = np.percentile(x, p)
        feature_dict[f'{pre}perc{100-p:02}'] = high = np.percentile(x, 100-p)
        feature_dict[f'{pre}iqr{p:02}'] = high - low

    xx = np.linspace(0,1,len(x))
    try:
        feature_dict[f'{pre}linear'] = np.polyfit(xx, x, 1)[0]
    except np.linalg.LinAlgError:
        feature_dict[f'{pre}linear'] = np.nan
    try:
        feature_dict[f'{pre}max_pos'] = xx[np.argmax(x)]
    except:
        feature_dict[f'{pre}max_pos'] = np.nan

    feature_dict[f'{pre}kurtosis'] = stats.kurtosis(x)
    feature_dict[f'{pre}skew'] = stats.skew(x)
    
    for m in [3,4,5]:
        feature_dict[f'{pre}moment_{m}'] = stats.moment(x,m)
    for k in [1,2,3]:
        feature_dict[f'{pre}kstats_{m}'] = stats.kstat(x,k)

    feature_dict[f'{pre}PE_31'] = perm_entropy(x, order=3, delay=1, normalize=False)
    feature_dict[f'{pre}PE_42'] = perm_entropy(x, order=4, delay=2, normalize=False)
    feature_dict[f'{pre}PE_48'] = perm_entropy(x, order=4, delay=8, normalize=False)

    for r in [None, (-128,128), (-32,32)]:
        for bins in [128, 64]:
            probs, bin_edges = np.histogram(x, bins=bins, range=r, density=True)
            feature_dict[f'{pre}BE_{r[1] if r else "auto"}_{bins}'] = -sum(p * np.log(p) for p in probs if p != 0)
    for p in [90, 95, 98, 99]:
        feature_dict[f'{pre}BE_p{p}'] = fc.binned_entropy(x, p)

    feature_dict[f'{pre}mean_change_rate'] = calc_change_rate(x)

    for peak in [5,8,10,12,15,20]:
        feature_dict[f'{pre}num_peaks_{peak}'] = fc.number_peaks(x, peak)

    return feature_dict


def make_feature_dict(x):
    x, noise = pre_process_seg(x, wiener_return_noise=True)

    feature_dict = dict()

    s = pd.Series(x)
    abs_x = np.abs(x)

    feature_dict['std'] = np.std(x) # should be proportional to sqrt of energy as mean=0
    #feature_dict['mean'] = np.mean(x) # should always be zero (after preprocessing)
    
    if noise is not None:
        feature_dict['noise_std'] = np.std(noise)
        abs_n = np.abs(noise)
        feature_dict['noise_q05'] = np.quantile(abs_n, 0.05)
        feature_dict['noise_q10'] = np.quantile(abs_n, 0.10)
        feature_dict['noise_median'] = np.median(abs_n)
        feature_dict['noise_q90'] = np.quantile(abs_n, 0.90)
        feature_dict['noise_q95'] = np.quantile(abs_n, 0.95)
        feature_dict['noise_perm_e'] = perm_entropy(noise, order=3, delay=1, normalize=False)
    
    feature_dict['median'] = np.median(x)
    feature_dict['q01'] = np.quantile(x, 0.01)
    feature_dict['q02'] = np.quantile(x, 0.02)
    feature_dict['q05'] = np.quantile(x, 0.05)
    feature_dict['q10'] = np.quantile(x, 0.10)
    feature_dict['q20'] = np.quantile(x, 0.20)
    feature_dict['q80'] = np.quantile(x, 0.80)
    feature_dict['q90'] = np.quantile(x, 0.90)
    feature_dict['q95'] = np.quantile(x, 0.95)
    feature_dict['q98'] = np.quantile(x, 0.98)
    feature_dict['q99'] = np.quantile(x, 0.99)
    feature_dict['max'] = np.max(x)
    feature_dict['min'] = np.min(x)
    feature_dict['iqr01'] = np.quantile(x, 0.99) - np.quantile(x, 0.01)
    feature_dict['iqr02'] = np.quantile(x, 0.98) - np.quantile(x, 0.02)
    feature_dict['iqr05'] = np.quantile(x, 0.95) - np.quantile(x, 0.05)
    feature_dict['iqr10'] = np.quantile(x, 0.90) - np.quantile(x, 0.10)
    feature_dict['iqr20'] = np.quantile(x, 0.80) - np.quantile(x, 0.20)
    feature_dict['ptp'] = np.ptp(x)
    
    feature_dict['kurtosis'] = stats.kurtosis(x)
    feature_dict['skew'] = stats.skew(x)
    
    feature_dict['moment_3th'] = stats.moment(x,3)
    feature_dict['moment_4th'] = stats.moment(x,4)
    feature_dict['moment_5th'] = stats.moment(x,5)
    feature_dict['kstats_1'] = stats.kstat(x,1)
    
    #feature_dict['std_first_1000'] = np.std(x[:1000])
    feature_dict['std_last_1k'] = np.std(x[-1000:])
    feature_dict['std_2ndlast_1k'] = np.std(x[-2000:-1000])
    feature_dict['std_last_5k'] = np.std(x[-5_000:])
    feature_dict['std_2ndlast_5k'] = np.std(x[-10_000:-5_000])
    #feature_dict['std_last_20k'] = np.std(x[-20_000:])

    ewm = pd.Series.ewm
    for span in [300,3000]:
        feature_dict[f'ewm_std_{span}_mean'] = ewm(s,span=span).std(skipna=True).mean(skipna=True)
        feature_dict[f'ewm_mean_{span}_std'] = ewm(s,span=span).mean(skipna=True).std(skipna=True)
    
    feature_dict['log_energy'] = np.log( np.dot(x,x) )
    
    for r in [None, (-128,128), (-32,32)]:
        probs, bin_edges = np.histogram(x, bins=128, range=r, density=True)
        feature_dict['binned_entropy_128'] = -sum(p * np.log(p) for p in probs if p != 0)
        probs, bin_edges = np.histogram(x, bins=64, range=r, density=True)
        feature_dict['binned_entropy_64'] = -sum(p * np.log(p) for p in probs if p != 0)
    for p in [95, 99]:
        feature_dict['binned_entropy_tsfresh_95'] = fc.binned_entropy(x, p)

    feature_dict['perm_entropy_31'] = perm_entropy(x, order=3, delay=1, normalize=False)
    feature_dict['perm_entropy_42'] = perm_entropy(x, order=4, delay=2, normalize=False)
    feature_dict['perm_entropy_48'] = perm_entropy(x, order=4, delay=8, normalize=False)

    feature_dict['abs_mean'] = np.mean(abs_x)
    feature_dict['abs_median'] = np.median(abs_x)

    feature_dict[f'mean_change_rate'] = calc_change_rate(x)
    l = 50_000
    for i in range(0,len(x),l):
        feature_dict[f'mean_change_rate_#{i}_{l}'] = calc_change_rate(x[i:i+l])

    for lag in [3,5,10]:
        feature_dict[f'autocorr_{lag}'] = fc.autocorrelation(x, lag)
    
    for win in [50,150,500,1000]:
        roll = s.rolling(win)
        for agg in ['mean', 'std']:
            rx = getattr(roll,agg)().dropna().values
            feature_dict[f'roll{win}{agg}_mean'] = np.mean(rx)
            feature_dict[f'roll{win}{agg}_std'] = np.std(rx)
            feature_dict[f'roll{win}{agg}_q20'] = np.quantile(rx, 0.20)
            feature_dict[f'roll{win}{agg}_q80'] = np.quantile(rx, 0.80)
            feature_dict[f'roll{win}{agg}_q05'] = np.quantile(rx, 0.05)
            feature_dict[f'roll{win}{agg}_q95'] = np.quantile(rx, 0.95)
            feature_dict[f'roll{win}{agg}_iqr80'] = np.quantile(rx, 0.80) - np.quantile(rx, 0.20)
            feature_dict[f'roll{win}{agg}_iqr95'] = np.quantile(rx, 0.95) - np.quantile(rx, 0.05)
            feature_dict[f'roll{win}{agg}__perm_e'] = perm_entropy(rx, order=3, delay=1, normalize=False)
    
    # Welch based features (supposably better than FFT)
    freq, pxx = signal.welch(x, nperseg=1024)
    for i in range(20,120+1,20):
        i0, i1 = i-20+1, i+1
        feature_dict['welch_mean_{:03d}_{:03d}'.format(i0,i1)] = np.mean(pxx[i0:i1])
        feature_dict['welch_std_{:03d}_{:03d}'.format(i0,i1)] = np.std(pxx[i0:i1])
    welch_use = 120
    pxx = pxx[:welch_use]
    feature_dict['welch_centroid'] = np.average(np.arange(welch_use), weights=pxx)
    feature_dict['welch_kurtosis'] = stats.kurtosis(pxx)
    feature_dict['welch_skew'] = stats.skew(pxx)

    zc = np.fft.fft(x)
    for name, a in [('FFTr',np.real(zc)), ('FFTi',np.imag(zc)), ('FFTabs',np.abs(zc))]:
        feature_dict[f'{name}_std'] = np.std( a )
        feature_dict[f'{name}_mean'] = np.mean( a )
        for nsta,nlta in [(100,300), (100,1000), (100,3000), (500,10_000)]:
            sta_lta = classic_sta_lta(a, nsta, nlta)
            feature_dict[f'{name}_sta{nsta}_lta{nlta}_mean'] = np.mean( sta_lta )
            feature_dict[f'{name}_sta{nsta}_lta{nlta}_std'] = np.std( sta_lta )
            for q in [90,95,98,99]:
                feature_dict[f'{name}_sta{nsta}_lta{nlta}_q{q:02d}'] = np.percentile( sta_lta, q )
    
    hop_length = 1001
    for f in [lf.spectral_centroid, lf.spectral_bandwidth, lf.spectral_flatness, lf.spectral_rolloff,
              lf.spectral_contrast, lf.zero_crossing_rate]:
        kwargs = dict(hop_length=hop_length)
        if f.__name__ == 'spectral_contrast':
            kwargs['linear'] = True
        feature_dict['{}_mean'.format(f.__name__)] = np.mean(f(x, **kwargs))
        feature_dict['{}_std'.format(f.__name__)] = np.std(f(x, **kwargs))
        feature_dict['{}_min'.format(f.__name__)] = np.min(f(x, **kwargs))
        feature_dict['{}_max'.format(f.__name__)] = np.max(f(x, **kwargs))

    autocorr = lf.rhythm.autocorrelate(x)
    feature_dict['autocorr_std'] = np.std(autocorr)
    feature_dict['autocorr_iqr05'] = np.quantile(autocorr, 0.95) - np.quantile(autocorr, 0.05)
    feature_dict['autocorr_iqr10'] = np.quantile(autocorr, 0.90) - np.quantile(autocorr, 0.10)
    feature_dict['autocorr_median'] = np.median(autocorr)
    feature_dict['autocorr_perm_e'] = perm_entropy(autocorr, order=3, delay=1, normalize=False)
    
    for th in [3,5,10,20,50,100]:
        feature_dict['above_%d'%th] = np.sum( abs_x > th )

    thresholds = [0.1, 0.2, 1/3, 0.5, 3/2, 0.85]
    for nsta,nlta in [(100,300), (100,1000), (100,3000), (500,10_000), (5000,2*len(x)//3)]:
        sta_lta = classic_sta_lta(x, nsta, nlta)
        # trigger_name = classic_sta_lta.__name__
        for th in thresholds:
            th *= nlta/nsta
            feature_dict['sta{}_lta{}_triggered_@{:.1f}'.format(nsta,nlta,th)] = \
                    np.sum( (sta_lta[:-1]<th) & (sta_lta[1:]>=th) )
        feature_dict['sta{}_lta{}_mean'.format(nsta,nlta)] = np.mean( sta_lta )
        feature_dict['sta{}_lta{}_std'.format(nsta,nlta)] = np.std( sta_lta )
        for q in [90,95,98,99]:
            feature_dict['sta{}_lta{}_q{:02d}'.format(nsta,nlta,q)] = np.percentile( sta_lta, q )

    feature_dict['num_peaks_librosa'] = len(librosa.util.peak_pick(
        x,
        pre_max=300, post_max=300,
        pre_avg=500, post_avg=500,
        delta=20, wait=300,
    ))
    for peak in [5,10,20]:
        feature_dict[f'num_peaks_{peak}'] = fc.number_peaks(x, peak)
    
    return feature_dict

f = make_feature_dict( np.random.rand(SEG_LEN) )
FEATURES = set( f.keys() )
N_FEATURES = len( f )
del f

