__all__ = [
        'GOOD_SPLITS', 'LANL_CV',
        'cross_validate',
        'get_grps_props', 'create_grp_split', 'get_good_split',
]

import numpy as np
import pandas as pd
import copy, sys, time
import sklearn

from .info import GRP_INFO
from .env import tqdm

GOOD_SPLITS = {
    3: [
        [[0, 2, 4, 5, 10, 15], [1, 6, 9, 11, 14], [3, 7, 8, 12, 13, 16]],
        [[0, 2, 3, 4, 11, 15], [1, 5, 7, 8, 13], [6, 9, 10, 12, 14, 16]],
        [[0, 5, 7, 8, 10, 13, 16], [1, 2, 3, 11, 15], [4, 6, 9, 12, 14]],
        [[0, 7, 9, 10, 12, 13], [1, 2, 4, 6, 8], [3, 5, 11, 14, 15, 16]],
    ],
    4: [
        [[0, 1, 4, 10], [2, 3, 11, 15], [5, 6, 9, 14, 16], [7, 8, 12, 13]],
        [[0, 1, 4, 11], [2, 3, 12, 15, 16], [5, 7, 8, 13], [6, 9, 10, 14]],
        [[0, 4, 10, 11], [1, 6, 13, 14], [2, 3, 5, 9, 16], [7, 8, 12, 15]],
        [[0, 4, 10, 15, 16], [1, 2, 3, 8], [5, 6, 7, 9], [11, 12, 13, 14]],
        [[0, 4, 10, 15, 16], [1, 2, 3, 5], [6, 7, 12, 13], [8, 9, 11, 14]],
    ],
    5: [
        [[0, 1, 4, 9], [2, 12, 13], [3, 6, 7], [5, 14, 15], [8, 10, 11, 16]],
        [[0, 3, 7, 8], [1, 10, 11], [2, 12, 15], [4, 5, 9, 16], [6, 13, 14]],
        [[0, 6, 7, 8], [1, 10, 11], [2, 9, 13], [3, 4, 12, 16], [5, 14, 15]],
        [[0, 7, 8, 12], [1, 9, 10], [2, 3, 13, 16], [4, 11, 15], [5, 6, 14]],
    ],
    6: [
        [[0, 1, 10], [2, 12, 13], [3, 8, 14], [4, 15], [5, 6, 7], [9, 11, 16]],
        [[0, 2, 5], [1, 11, 16], [3, 10, 12], [4, 9, 13], [6, 7, 8], [14, 15]],
        [[0, 7, 12], [1, 3, 15], [2, 6, 8], [4, 13, 16], [5, 10, 11], [9, 14]],
    ],
    7: [
        [[0, 3, 7], [1, 6, 8], [2, 5], [4, 15], [9, 14], [10, 13], [11, 12, 16]],
        [[0, 5, 7], [1, 9], [2, 15], [3, 11, 13], [4, 8], [6, 14], [10, 12, 16]],
        [[0, 5, 7], [1, 12], [2, 13], [3, 14], [4, 8], [6, 9, 15, 16], [10, 11]],
        [[0, 7, 8], [1, 5, 6], [2, 15], [3, 14], [4, 12], [9, 10], [11, 13, 16]],
        [[0, 7, 8], [1, 11], [2, 12], [3, 15, 16], [4, 13], [5, 6, 10], [9, 14]],
        [[0, 7, 12], [1, 6, 8], [2, 3], [4, 9], [5, 10, 16], [11, 15], [13, 14]],
    ],
}

class LANL_CV:
    '''
    Cross validation strategy for the LANL Earthquake Prediction challenge training set.
    
    It is quite similar to `sklearn.model_selection.GroupKFold`; one defines the groups
    to be held out for each split.
    
    Args:
        splits (list, int, None):
                        A list of lists of group indices (or a list of such). At each split
                        the corresponing list of group indices is held out. It is recommended
                        to use one of the `lanl.cv.GOOD_SPLITS`.
                        * list: A list of list of group indices, which define the splits.
                                Alternatively, it can be a list of such (e.g.
                                `lanl.cv.GOOD_SPLITS[4]`).
                        * None (default):
                                Then `split` defaults to `lanl.cv.GOOD_SPLITS[5]`.
                        * int:  Choose randomly `repeat` instances of
                                `lanl.cv.GOOD_SPLITS[splits]` (with replacement, if
                                `repeats` is larger than there are "good splits" defined).
        repeats (int):  The number of times the iteration of the list `splits` is repeated,
                        in case it is a simple list of list of group indices. If `splits` is
                        a list of such, the argument `repeats` is ignored. If `splits` is an
                        `int`, however, it defines the number of splits chosen from
                        `lanl.cv.GOOD_SPLITS[splits]` ().
        shuffle (bool): Whether to shuffle train and test set indices (independently, of
                        course) in each split.
        verbose (int):  Verbosity level.
    '''
    
    def __init__(self, splits=None, repeats=1, shuffle=False, verbose=0):
        repeats = int(repeats)
        if splits is None:
            splits = GOOD_SPLITS[5]

        if isinstance(splits, int):
            good_splits = GOOD_SPLITS[splits]
            replace = ( repeats > len(good_splits) )
            idx = np.random.choice(len(good_splits),size=repeats,replace=replace)
            splits = [ good_splits[i] for i in idx ]
        elif isinstance(splits, list):
            if not all(isinstance(s,list) for s in splits):
                raise TypeError('`splits` is not a `list` of `list`s!')
            s = splits[0][0]
            if isinstance(s,int):
                splits = [ splits ] * repeats
        else:
            raise TypeError('`splits` is neither of type `int` nor `list`!')
        self._validate_splits(splits)
        self._splits = copy.deepcopy(splits)
        self._shuffle = shuffle
        self._verbose = int(verbose)
    
    def _validate_splits(self, splits):
        if not isinstance(splits, list):
            raise TypeError("`splits` is not of correct type!")
        for split in splits:
            if not isinstance(split, list):
                raise TypeError("`splits` is not of correct type!")
            for grp in split:
                if not isinstance(split, list):
                    raise TypeError("`splits` is not of correct type!")
                if not all( isinstance(i,int) for i in grp ):
                    raise TypeError("`splits` is not of correct type!")
                if len(grp) != len(set(grp)):
                    print(sys.stderr, "WARNING: a given split has repeated indices in a group!")
                if set(grp) - set(range(17)):
                    raise ValueError(f"There are invalid group indices in a split: {set(grp)-set(range(17))}")
            as_sets = list(map(set, split))
            for i,s1 in enumerate(as_sets):
                for s2 in as_sets[i+1:]:
                    if s1 & s2:
                        print(sys.stderr, f"WARNING: some indices are in multiple groups: {s1&s2}")
    
    @property
    def shuffle(self):
        return self._shuffle
        
    def get_n_splits(self, X=None, y=None, groups=None):
        '''
        Returns the number of splitting iterations in the cross-validator.
        
        Args:
            X:      Always ignored, exists for compatibility.
            y:      Always ignored, exists for compatibility.
            groups: Always ignored, exists for compatibility.
        
        Returns:
            n_splits (int): Returns the number of splitting iterations in
                            the cross-validator.
        '''
        return sum( len(s) for s in self._splits )

    def split(self, X, y, groups, verbose=None):
        '''
        Generate indices to split data into training and test set.
        
        Args:
            X (array-like, shape (n_samples, n_features)):
                    Training data, where n_samples is the number of samples
                    and n_features is the number of features.
            y (array-like, shape (n_samples,)):
                    The target variable for supervised learning problems.
            groups (array-like, with shape (n_samples,)):
                    Group labels for the samples used while splitting the
                    dataset into train/test set.
            verbose (int):
                    Verbosity level.
        
        Yields:
            train_idx (np.ndarray): The training set indices for that split.
            test_idx (np.ndarray):  The testing set indices for that split.
        '''
        if verbose is None:
            verbose = self._verbose
        verbose = int(verbose)

        cnt = 0
        for split in self._splits:

            cv_groups = np.empty(len(groups), int)
            for i, grps in enumerate(split):
                for g in grps:
                    cv_groups[groups==g] = i

            for n in range(len(split)):
                cnt += 1
                if verbose > 0:
                    print(f"CV split {cnt:2d} of {self.get_n_splits()}")

                train_idx = np.where(cv_groups!=n)[0]
                test_idx  = np.where(cv_groups==n)[0]
                if self._shuffle:
                    np.random.shuffle(train_idx)
                    np.random.shuffle(test_idx)
                yield train_idx, test_idx

def cross_validate(model, X, y, groups, shuffle=False,
                   fit_params=None, verbose=1, n_splits=5):
    '''
    Cross validate a given model with the LANL specific CV strategy.

    Args:
        model (estimator : estimator object implementing `fit`):
                            Passed at is to tke scitkit-learn `cross_validate`.
        X (array-like):     The data to fit.
        y (array-like):     The target variable.
        groups (array-like):Group labels of the segments between the quakes.
        shuffle (bool):     Whether to shuffle the training data at each split.
        fit_params (bool):  The parameters to pass to the `fit` routine of the model.
        verbose (int):      The verbosity level. (And `verbose-1` is passed to
                            `sklearn.model_selection.cross_validate`.)
        n_splits (int):     The number of splits of `lanl.cv.GOOD_SPLITS` to iterate over.

    Returns:
        scores (dict):      Array of scores like scikit-learn's `cross_validate` returns
                            with 'test_MAE', 'train_MAE', 'fit_time', and 'score_time'.
                            Those are, however, averaged over the individual splits, i.e.
                            the arrays have a length `n_splits`.
    '''
    verbose = int(verbose)

    t_start = time.time()
    scores = {
        'test_MAE': [],
        'train_MAE': [],
        'fit_time': [],
        'score_time': [],
    }
    pbar = tqdm(GOOD_SPLITS[n_splits], disable=(not verbose))
    for i,splits in enumerate(pbar):
        indiv_scores = sklearn.model_selection.cross_validate(
            model,
            X, y, groups,
            scoring='neg_mean_absolute_error',
            cv=LANL_CV(splits, shuffle=shuffle),
            fit_params=fit_params,
            return_train_score=True,
            n_jobs=-1,
            verbose=max(0,verbose-1),
        )
        scores['test_MAE'].append( -np.mean(indiv_scores['test_score']) )
        scores['train_MAE'].append( -np.mean(indiv_scores['train_score']) )
        # do I want the mean for the following ?
        scores['fit_time'].append( np.mean(indiv_scores['fit_time']) )
        scores['score_time'].append( np.mean(indiv_scores['score_time']) )
        if verbose:
            pbar.set_postfix(ordered_dict={'<MAE>':np.mean(scores['test_MAE'])})
    for key in list(scores.keys()):
        scores[key] = np.array(scores[key])
    t = time.time() - t_start

    if verbose:
        MAE = scores['test_MAE']
        print(f"CV'ed {len(GOOD_SPLITS[n_splits])} times with {n_splits} splits in {t:.1f} sec total:")
        print(f"  MAE = {np.mean(MAE):.4f} +- {np.std(MAE,ddof=1):.4f}")

    return scores

def get_grps_props(grps):
    '''
    Get fraction of duration and mean TTF for group of groups.
    
    Args:
        grps (pd.DataFrame):    A selection of rows of `lanl.info.GRP_INFO`
                                that build the group of groups.
    
    Returns:
        frac (float):       The fraction of the duration these groups have
                            of the total training sequence.
        mean_TTF (float):   The mean time to failure in the group of groups.
    '''
    if len(grps) == 0:
        return 0.0, np.nan
    frac = grps['frac_duration'].sum()
    mean_TTF = np.average(grps['mean_TTF'], weights=grps['frac_duration'])
    return frac, mean_TTF

def create_grp_split(min_frac=0.1):
    '''
    Return DataFrame with split of the training data, in this case a "grouping of the groups".
    
    Args:
        min_frac (float):    The minimum fraction of the duration a group of groups must have.
    
    Returns:
        split (pd.DataFrame): The table with information about the groups.
    '''
    grps = GRP_INFO.iloc[ np.random.permutation(GRP_INFO.index) ]
    
    left_frac, left_mean_TT = get_grps_props(grps)
    splits = []
    while left_frac >= min_frac:
        for n in range(1,len(grps)+1):
            split = grps[:n]
            frac, mean_TTF = get_grps_props(split)
            if frac >= min_frac:
                splits.append( [split, n, frac, mean_TTF] )
                grps = grps[n:]
                break
        left_frac, left_mean_TT = get_grps_props(grps)
    splits = pd.DataFrame(splits, columns=['split', 'N', 'frac', 'mean_TTF'])
    
    for _,g in grps.iterrows():
        i_min = np.argmin(splits['frac'])
        split = splits.iloc[i_min]['split'].append(g)
        frac, mean_TTF = get_grps_props(split)
        splits.iloc[i_min] = [split, len(split), frac, mean_TTF]
    assert np.sum(splits['N']) == 17
    assert np.allclose(splits['frac'].sum(), 1.0)
    
    splits['split'] = [np.array(s.index) for s in splits['split']]
    
    return splits

def get_good_split(min_frac=0.1, max_frac_ptp=0.05, max_TTF_ptp=None, N=100, verbose=2):
    '''
    Find a split with given requirements.
    
    Args:
        min_frac (float):       The minimum fraction in duration of a group
                                of groups.
        max_frac_ptp (float):   The maximum difference between the duration
                                fractions of the groups of groups for the split.
        max_TTF_ptp (float):    The maximum difference between the mean time to
                                failure of the groups of groups.
        N (int):                The number of random trials by `create_grp_split`
                                to try to generate to choose the best split from.
        verbose (int):          The verbosity level: 0 - no output, 1 - print
                                stats for result, 2 - use tqdm in addition.
    
    Returns:
        split (pd.DataFrame):   A Table with rows for each group of groups:
                                - split:    np.ndarray[int] of the groups.
                                - N:        Number of groups (i.e. len(split)).
                                - frac:     The fraction of total duration of
                                            this group of groups.
                                - mean_TTF: The mean time to failure of this
                                            group of groups.
    '''
    verbose = int(verbose)
    best_split = (None, np.inf)
    for i in tqdm(range(N), disable=(verbose<2)):
        splits = create_grp_split(min_frac=min_frac)
        score = np.std(splits['mean_TTF'])
        if score < best_split[1] and np.ptp(splits['frac']) <= max_frac_ptp:
            if max_TTF_ptp is None or np.ptp(splits['mean_TTF']) <= max_TTF_ptp:
                best_split = (splits, score)
    
    if verbose:
        if best_split[0] is None:
            print(f"ERROR: No split with requirements found in {N} iterations.")
        else:
            print(f"best split with requirements found in {N} iterations:")
            print(f"  N={len(best_split[0]):2d} -- "
                  f"std(<TTF>)={best_split[1]:.2f} -- "
                  f"std(frac)={np.std(best_split[0]['frac']):.1%} -- "
                  f"ptp(frac)={np.ptp(best_split[0]['frac']):.1%}")

    return best_split[0]

