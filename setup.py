#!/usr/bin/env python

from distutils.core import setup

setup(
        name='LANL',
        version='1.0',
        description='Kaggles Challenge LANL',
        author='Bernhard Roettgers',
        author_email='bernhard.roettgers@yahoo.de',
        url='https://bitbucket.org/broett/lanl/src/master/',
        packages=['lanl'],
        install_requires=['numpy', 'pandas', 'scipy', 'tqdm', 'joblib', 'librosa', 'obspy'],
)

